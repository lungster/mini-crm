<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Laravel5Helpers\Exceptions\LaravelHelpersExceptions;
use App\Repositories\Company as Repository;
use App\Definitions\Company as Definition;
use App\Mail\NewCompanyNotification as Email;

class Company extends Controller
{
    public function store(Request $request)
    {
        try {
            $message = 'Record created successfully';
            $this->upload($request);
            $this->sendMail();
            return $this->sendToView('success', 'success', ['message' => $message]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);
        }
    }

    public function sendMail()
    {
        return Mail::to('imadecode@gmail.com')->send(new Email());
    }

    public function show($id)
    {
        try {
            $company = $this->getRepository()->getResource($id);

            return $this->sendToView('admin.company', 'employee', ['company' => $company]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);

        }
    }

    public function upload(Request $request)
    {
        try {
            $path = $request->file('logo')->store('logos', 'public');
            return $this->getRepository()->createResource($this->getDefinition($request, $path));
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);
        }
    }

    public function edit($id)
    {
        try {
            $company = $this->getRepository()->getResource($id);

            return $this->sendToView('admin.edit-company', 'employee', ['company' => $company]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $message = 'Details updated successfully';
            $this->getRepository()->editResource($this->getDefinition($request), $id);

            return $this->sendToView('success', 'success', ['message' => $message]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);
        }
    }


    public function destroy($id)
    {
        try {
            $message = 'Record deleted successfully';
            $this->getRepository()->deleteResource($id);

            return $this->sendToView('success', 'success', ['message' => $message]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);
        }
    }

    private function getRepository()
    {
        return new Repository;
    }

    private function getDefinition(Request $request, $path)
    {
        $name = $request['name'];
        $email = $request['email'];
        $website = $request['website'];
        return new Definition($name, $email, $website, $path);
    }

    private function getEmail()
    {
        return new Email;
    }
}

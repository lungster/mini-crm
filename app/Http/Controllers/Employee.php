<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Definitions\Employee as Definition;
use App\Repositories\Employee as Repository;
use Laravel5Helpers\Exceptions\LaravelHelpersExceptions;

class Employee extends Controller
{

    public function store(Request $request)
    {
        try {
            $message = 'Employee Added Successfully';

            $this->getRepository()->createResource($this->getDefinition($request));

            return $this->sendToView('success', 'success', ['message' => $message]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);
        }
    }


    public function show($id)
    {
        try {
            $employee = $this->getRepository()->getResource($id);

            return $this->sendToView('admin.employee', 'employee', ['employee' => $employee]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);

        }
    }


    public function edit($id)
    {
        try {
            $employee = $this->getRepository()->getResource($id);

            return $this->sendToView('admin.edit-employee', 'employee', ['employee' => $employee]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);
        }
    }


    public function update(Request $request, $id)
    {
        try {
            $message = 'Details updated successfully';
            $this->getRepository()->editResource($this->getDefinition($request), $id);

            return $this->sendToView('success', 'success', ['message' => $message]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);
        }
    }


    public function destroy($id)
    {
        try {
            $message = 'Record deleted successfully';
            $this->getRepository()->deleteResource($id);

            return $this->sendToView('success', 'success', ['message' => $message]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendToView('error', 'error', ['message' => $exception->getMessage()]);
        }
    }

    protected function getRepository()
    {
        return new Repository;
    }

    protected function getDefinition(Request $employee)
    {
        return new Definition($employee->all());
    }
}

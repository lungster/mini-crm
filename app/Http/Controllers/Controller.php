<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function sendToView($view, $title, array $variables = [])
    {
        $attributes = array_merge(['title' => $title], $variables);

        return view($view, $attributes);
    }

    protected function sendError($message)
    {
        Session::flash('error', $message);

        return Redirect::back();
    }
}

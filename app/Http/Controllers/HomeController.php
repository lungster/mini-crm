<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Employee as EmployeeRepo;
use App\Repositories\Company as CompanyRepo;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function admin()
    {
        $employees = $this->getEmployees()->getPaginated();
        $companies = $this->getCompanies()->getPaginated();
        return $this->sendToView('admin.index', 'Admin', ['employees' => $employees, 'companies' => $companies]);
    }

    protected function getEmployees()
    {
        return new EmployeeRepo;
    }

    protected function getCompanies()
    {
        return new CompanyRepo;
    }
}

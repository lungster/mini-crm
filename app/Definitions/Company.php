<?php


namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;

class Company extends Definition
{
    public $name;

    public $email_address;

    public $website;

    public $logo;

    public function __construct($name, $email, $website, $path)
    {
        parent::__construct([]);

        $this->name = $name;
        $this->email_address = $email;
        $this->website = $website;
        $this->logo = $path;
    }

    protected function setValidators()
    {
        return $this->validators = [
            'name' => 'required',
            'website' => 'required',
            'email_address' => 'required'
        ];
    }

}
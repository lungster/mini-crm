<?php


namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;

class Employee extends Definition
{
    public $first_name;

    public $last_name;

    public $email_address;

    public $phone_number;

    public $company;

    protected function setValidators()
    {
        return $this->validators = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email_address' => 'required'
        ];
    }

}
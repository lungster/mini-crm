<?php


namespace App\Models;


use Laravel5Helpers\Uuid\UuidModel;

class Employee extends UuidModel
{
    protected $table = 'employees';

}
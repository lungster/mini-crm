<?php


namespace App\Models;


use Laravel5Helpers\Uuid\UuidModel;

class Company extends UuidModel
{
    protected $table = 'companies';

    public function getPath()
    {
        return asset('/storage/' . $this->logo);
    }

}
<?php

namespace App\Api\Controllers;

use Illuminate\Http\Request;
use App\Definitions\Employee as Definition;
use App\Repositories\Employee as Repository;
use Laravel5Helpers\Exceptions\LaravelHelpersExceptions;
use App\Api\Response\Response;
use App\Api\Response\TransFormedResponse;

class Employee extends Controller
{

    public function all()
    {
        try {
            $employees = $this->getRepository()->getPaginated();
            $data = [];

            foreach ($employees as $employee) {
                $data[] = new Response($employee);
            }

            return $this->sendSuccess(new TransFormedResponse($employees, $data));
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendSuccess($exception->getMessage());
        }
    }

    protected function getRepository()
    {
        return new Repository;
    }

    protected function getDefinition(Request $employee)
    {
        return new Definition($employee->all());
    }
}

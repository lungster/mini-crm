<?php

namespace App\Api\Response;

use Laravel5Helpers\Definitions\Definition;

use App\Models\Employee as Model;

class Response extends Definition
{
    public $first_name;

    public $last_name;

    public $email_address;

    public $company;

    public function __construct(Model $employee)
    {
        parent::__construct($employee->toArray());

    }

    protected function setValidators()
    {
        // TODO: Implement setValidators() method.
    }
}

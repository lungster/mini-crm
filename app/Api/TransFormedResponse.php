<?php

namespace App\Api\Response;

use Illuminate\Pagination\LengthAwarePaginator;

final class TransFormedResponse
{
    public $total;

    public $per_page;

    public $current_page;

    public $items = [];

    public $next_page;

    public $prev_page;

    public function __construct(LengthAwarePaginator $paginator, $data = [])
    {
        $this->total = $paginator->total();

        $this->per_page = $paginator->perPage();

        $this->current_page = $paginator->currentPage();

        $this->prev_page = ($this->current_page - 1);

        $this->next_page = $this->getNextPage($paginator);


        $this->items = $data;
    }

    private function getNextPage(LengthAwarePaginator $paginator)
    {
        $page = $this->current_page + 1;

        if ($this->current_page == $paginator->lastPage()) {
            return 0;
        }

        if ($page > $paginator->lastPage()) {
            return 0;
        }

        return $page;
    }
}

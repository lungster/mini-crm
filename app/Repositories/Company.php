<?php


namespace App\Repositories;

use Laravel5Helpers\Repositories\Repository;
use App\Models\Company as Model;

class Company extends Repository
{
    protected $pageSize = 3;

    protected function getModel()
    {
        return new Model;
    }
}

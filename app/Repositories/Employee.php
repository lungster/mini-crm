<?php


namespace App\Repositories;

use Laravel5Helpers\Repositories\Repository;
use App\Models\Employee as Model;

class Employee extends Repository
{
    protected $pageSize = 3;

    protected function getModel()
    {
        return new Model;
    }
}

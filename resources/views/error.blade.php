@extends('layouts.app')

@section('content')
    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-danger text-center">
                            <h2> {{ $message }} </h2>
                        </div>
                        <a href="/admin" class="btn btn-danger"> Back</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
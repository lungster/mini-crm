@component('mail::message')
    New Company added

    Hi Admin, New company has been added
    @component('mail::button', ['url' => 'http://crm.test/admin'])
        Go to your Dashboard
    @endcomponent
    Sincerely,
    CRM App
@endcomponent
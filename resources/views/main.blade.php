@extends('layouts.app')

@section('content')
    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <div class="col-md-10 mb-5">
                <div class="border p-3 mb-3 bg-white">
                    <div class="section-title mb45 headline text-center mb-5">
                        <span class="subtitle text-uppercase">Application Name</span>
                        <h2>Mini<span> CRM - CRUID</span></h2>
                    </div>
                </div>
            </div>

            <div class="col-md-10 mt-4">
                <div class="card">
                    <div class="card-body p-5 divBlock">
                        <div class="row justify-content-center">
                            <div class="col-md-4">
                                <div class="text-center shadow-lg">
                                    <a href="/admin" class="btn btn-outline-light btn-block"> Admin Login</a>
                                </div>
                                {{--{{ route('login') }}--}}
                            </div>
                        </div>
                        <div class="border p-3 m-5 row shadow-sm">
                            <p class="p-6 text-white">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit esse cillum dolore eu

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit esse cillum dolore eu
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@extends('layouts.app')

@section('content')
    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <div class="col-md-10 mb-5">
                <div class="border p-3 mb-3 bg-white">
                    <div class="section-title mb45 headline text-center mb-5">
                        <span class="subtitle text-uppercase">Profile Of</span>
                        <h2>{{ $employee->first_name }}<span> {{ $employee->last_name }} </span></h2>
                    </div>
                </div>
            </div>

            <div class="col-md-10">
                <div class="card">
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-md-10 pt-3">
                                <div class="mb-3"><strong>Full Name</strong></div>
                                <div class="mb-3"><strong>Phone Number</strong></div>
                                <div class="mb-3"><strong>Email Address</strong></div>
                                <div class="mb-3"><strong>Company Name</strong></div>
                                <div class="mb-3"><strong>Date Added</strong></div>
                            </div>
                            <div class="col-md-2 pt-3">
                                <div class="mb-3">{{ $employee->first_name }} {{ $employee->last_name }} </div>
                                <div class="mb-3">{{ $employee->phone_number }} </div>
                                <div class="mb-3">{{ $employee->email_address }} </div>
                                <div class="mb-3">{{ $employee->company }} </div>
                                <div class="mb-3">{{ $employee->created_at }} </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/admin" class="btn btn-danger mt-3"> Back</a>

            </div>
        </div>
    </div>
@stop
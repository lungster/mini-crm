@extends('layouts.app')

@section('content')
    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <div class="col-md-10 mb-5">
                <div class="border p-3 mb-3 bg-white">
                    <div class="section-title mb45 headline text-center mb-5">
                        <span class="subtitle text-uppercase">Admin Dashboard</span>
                        <h2>Manage<span> Employees & Companies </span></h2>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="jumbotron row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="border-bottom font-weight-bold"> Employees</div>
                                @if($employees->count())

                                    <div class="row mb-3">
                                        @foreach($employees as $employee)
                                            <div class="col-md-10 pt-3">
                                                <div>
                                                    - <a href="/employee/{{ $employee->uuid }}">
                                                        {{ $employee->first_name }} {{ $employee->last_name }}
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-2 pt-3">
                                                <div>
                                                    <form method="post" action="/employee/{{ $employee->uuid }}">
                                                        <a href="/employee/{{ $employee->uuid }}/edit"
                                                           class="btn btn-outline-dark btn-sm"><i
                                                                    class="fa fa-pencil"></i>
                                                        </a>
                                                        <button type="submit" class="btn btn-outline-dark btn-sm">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                        @method('DELETE')
                                                        @csrf
                                                    </form>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="mb-3 border-top pt-2">
                                        {{ $employees->links() }}
                                    </div>
                                @else
                                    <div class="alert alert-danger mt-3">
                                        <h4>There's currently no employees</h4>
                                    </div>
                                @endif
                            </div>
                            <div class="card-footer">
                                <a href="#" data-toggle="modal" data-target="#add-employee"
                                   class="btn btn-outline-dark float-right"> Add</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="border-bottom font-weight-bold"> Companies</div>
                                @if($companies->count())

                                    <div class="row">
                                        @foreach($companies as $company)
                                            <div class="col-md-10 pt-3">
                                                <div>
                                                    - <a href="/company/{{ $company->uuid }}">
                                                        {{ $company->name }}
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-2 pt-3">
                                                <div>
                                                    <form method="post" action="/company/{{ $company->uuid }}">
                                                        <a href="/company/{{ $company->uuid }}/edit"
                                                           class="btn btn-outline-dark btn-sm"><i
                                                                    class="fa fa-pencil"></i>
                                                        </a>
                                                        <button type="submit" class="btn btn-outline-dark btn-sm">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                        @method('DELETE')
                                                        @csrf
                                                    </form>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="mb-3 border-top pt-2">
                                        {{ $companies->links() }}
                                    </div>
                                @else
                                    <div class="alert alert-danger mt-3">
                                        <h4>There's currently no companies</h4>
                                    </div>
                                @endif
                            </div>
                            <div class="card-footer">
                                <a href="#" data-toggle="modal" data-target="#add-company"
                                   class="btn btn-outline-dark float-right"> Add</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('includes.add-employee')
    @include('includes.add-company')
@stop
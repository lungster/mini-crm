@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <div class="col-md-10 mb-5">
                <div class="border p-3 mb-3 bg-white">
                    <div class="section-title mb45 headline text-center mb-5">
                        <span class="subtitle text-uppercase">Edit Details Of</span>
                        <h2>{{ $employee->first_name }}<span> {{ $employee->last_name }} </span></h2>
                    </div>
                </div>
            </div>

            <div class="col-md-10">
                <div class="card p-3">
                    <div class="card-body">
                        <form method="post" action="/employee/{{ $employee->uuid }}">
                            <div class="modal-body">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-user"></i> First name</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="first_name" value="{{ $employee->first_name }}"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-user-md"></i> Last
                                        name</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="last_name" value="{{ $employee->last_name }}"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-book"></i> Email Address
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="email" name="email_address" value="{{ $employee->email_address }}"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-phone"></i> Phone number
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" name="phone_number" value="{{ $employee->phone_number }}"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-building"></i> Company
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" name="company" value="{{ $employee->company }}"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                <input type="submit" value="Save" class="btn btn-danger bloodRed">
                            </div>
                            @method('PUT')
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

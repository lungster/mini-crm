@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <div class="col-md-10 mb-5">
                <div class="border p-3 mb-3 bg-white">
                    <div class="section-title mb45 headline text-center mb-5">
                        <span class="subtitle text-uppercase">Edit Details Of</span>
                        <h2>{{ $company->name }}</h2>
                    </div>
                </div>
            </div>

            <div class="col-md-10">
                <div class="card p-3">
                    <div class="card-body">
                        <form method="post" action="/company" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-building"></i> Company
                                        name</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="name" value="{{$company->name}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-book"></i> Email
                                        Address</label>
                                    <div class="col-sm-8">
                                        <input type="email" value="{{$company->email_address}}" placeholder="doe" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-globe"></i> Website link
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" name="website" value="{{$company->website}}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label"><i class="fa fa-file"></i> Logo </label>
                                    <div class="col-sm-8">
                                        <input type="file" name="logo" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                <input type="submit" value="Save" class="btn btn-danger bloodRed">
                            </div>
                    </div>
                </div>
                @csrf
                </form>

            </div>
        </div>
    </div>
    </div>
    </div>
@stop

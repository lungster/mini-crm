<div class="modal fade" id="add-employee" tabindex="-1" role="dialog"
     aria-hidden="true">
    <form method="post" action="/employee">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bloodRed">
                    <h5 class="modal-title">Add Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-user"></i> First name</label>
                        <div class="col-sm-8">
                            <input type="text" name="first_name" placeholder="john" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-user-md"></i> Last name</label>
                        <div class="col-sm-8">
                            <input type="text" name="last_name" placeholder="doe" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-book"></i> Email Address </label>
                        <div class="col-sm-8">
                            <input type="email" name="email_address" placeholder="name@domain.com" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-phone"></i> Phone number </label>
                        <div class="col-sm-8">
                            <input type="text" name="phone_number" placeholder="071 556 0000" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-building"></i> Company </label>
                        <div class="col-sm-8">
                            <input type="text" name="company" placeholder="facebook inc" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <input type="submit" value="Save" class="btn btn-danger bloodRed">
                </div>
            </div>
        </div>
        @csrf
    </form>

</div>
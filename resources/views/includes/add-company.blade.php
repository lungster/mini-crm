<div class="modal fade" id="add-company" tabindex="-1" role="dialog"
     aria-hidden="true">
    <form method="post" action="/company" enctype="multipart/form-data">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bloodRed">
                    <h5 class="modal-title">Add Company</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-building"></i> Company name</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" placeholder="facebook inc" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-book"></i> Email Address</label>
                        <div class="col-sm-8">
                            <input type="email" name="email" placeholder="doe" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-globe"></i> Website link </label>
                        <div class="col-sm-8">
                            <input type="text" name="website" value="https://" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><i class="fa fa-file"></i> Logo </label>
                        <div class="col-sm-8">
                            <input type="file" name="logo" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <input type="submit" value="Save" class="btn btn-danger bloodRed">
                </div>
            </div>
        </div>
        @csrf
    </form>

</div>
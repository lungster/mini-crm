<div class="modal fade" id="delete-employee-{{ $employee->uuid }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <form method="post" action="/employee/{{ $employee->uuid }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bloodRed">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label> Type DETELE to Confirm</label>
                    <input type="text" name="confirm" class="form-control" required>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <input type="submit" value="OK" class="btn btn-danger bloodRed">
                </div>
            </div>
        </div>
        @method('DELETE')
        @csrf
    </form>

</div>